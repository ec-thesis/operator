#!/bin/bash
# Build + deploy the operator to k8s, s.t. it processes CRDs

set -eu

kubectl config use-context kind-kind 

make docker-build docker-push


# Run locally
# make install run

make undeploy ||:
#sleep 5

# Run in kind cluster
make install deploy
#sleep 5
#kubectl get pod --all-namespaces